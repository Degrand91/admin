'use strict';

module.exports.setResponseHeaders = function(res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Content-Type', 'application/json; charset=utf-8');
};

module.exports.getHeader = function(req, headerName) {
    return req && req.header && req.header[headerName];
};

module.exports.setHeader = function(res, headerName, headerValue) {
    res.header[headerName] = headerValue;
};

module.exports.safeJSONParse = function (jsonStr) {
    let jsonObj = null;
    try {
        jsonObj = JSON.parse(jsonStr);
    } catch(error) { console.log('Unable to parse body - ' + error); }
    return jsonObj;
}