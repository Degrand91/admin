'use strict';

const http = require('http');
const routes = require('./routes')();
let helper = require('./helper');
const port = 3333;
let httpServer = null;

startService();

function startService() {
    httpServer = http.createServer(handleRequests);
    httpServer.listen(port,'0.0.0.0');
    console.log("AUTH SERVICE RUNNING ON PORT: " +port);
}

function handleRequests(req, res) {
    res.setHeader("cache-Control", "no-cache"); // HTTP 1.1.
    res.setHeader("pragma", "no-cache"); // HTTP 1.0.
    res.setHeader("expires", "-1");
    helper.setResponseHeaders(res);
    routes.handleRoute(req, res);
}