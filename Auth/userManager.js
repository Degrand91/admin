'use strict';
module.exports = function () {
    let bcrypt = require('bcrypt');
    let usersDb = require('./usersDb')();

    function registerUser(username, password, email, callback) {
        let userData = {
            username: username,
            password: password,
            email: email
        };
        let isValidData = validateData(userData, ["email", "username", "password"]);
        if (!isValidData) return callback(JSON.stringify({ "status": -1, "message": "invalid Data" }));
        preparePassword(userData.password, function (error, newPsw) {
            if (error) return callback(JSON.stringify({ "status": -1, "message": "invalid Data" }));
            userData.password = newPsw;
            usersDb.registerUser(userData, callback);
        })
        
    }

    function validateData(data, fields){ //needs to be improved
        let conditions = [];
        if(fields.indexOf("email") >= 0){
            conditions.push(!data || !data.email || data.email.indexOf("@") < 0 || data.email.indexOf(".") < 0);
        }
        if(fields.indexOf("username") >= 0){
            conditions.push(!data || !data.username);
        }
        if(fields.indexOf("password") >= 0){
            conditions.push(!data || !data.password);
        }
        return !(conditions.indexOf(true) >= 0);
    }

    function preparePassword(plainPwd, callback) {
        if (!plainPwd)  return callback(defaultPassword);
        bcrypt.hash(plainPwd, 10, function(error, hash) {
            if (error){
                console.log('WARNING: Unable to generate password - ' + error);
                return callback(error,"defaultPassword");
            }
            return callback(null, hash);
        });
    }

    return {
        registerUser: registerUser
    }
};