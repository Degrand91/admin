'use strict';
module.exports = function() {
    let router = require('router')();
    let auth = require('./auth')();
    let userManager = require('./userManager')();
    let helper = require('./helper');

    router.post('/api/*', extractBody);
    router.post('/api/signin', authenticate);
    router.post('/api/signup', register);
    router.post('/api/check', auth.isAuthenticated);

    
    function extractBody(req, res, next) {
        let body = '';
        req.on('data', function(chunk) { body += chunk });
        req.on('end', function () {
            req.jsonBody = helper.safeJSONParse(body) || {};
            return next();
        });
        req.on('error', function(error) {
            req.jsonBody = {};
            console.log('Error extracting body - ' + error);
            return next();
        });
    }

    function register(req, res) {
        if (!req.jsonBody) {
            console.log(req.jsonBody);
            res.end(JSON.stringify({error : true, content: "Unable to register missing data"}));
        } else {
            console.log('Register request for user: ' + req.jsonBody.username);
            userManager.registerUser(req.jsonBody.username, req.jsonBody.password, req.jsonBody.email, function complete(messageStr) {
                res.end(messageStr);
            });
        }
    }

    function authenticate(req, res) {
        if (!req.jsonBody) {
            res.end({error : true, content: "Unable to login"});
        } else {
            let user = req.jsonBody.username;
            let password = req.jsonBody.password;
            console.log('Authenticate request for user: ' + user);
            auth.authenticate(user, password, authenticateComplete);
        }

        function authenticateComplete(error, token) {
            if (error) {
                console.log('login failed - ' + error);
                res.end(JSON.stringify({error: true, content : "Unable to Login - " + error.message}));
            } else {
                console.log('login successfull');
                res.end(JSON.stringify({error : false, content : token}));
            }
        }
    }
    
    return {
        handleRoute: function(request, response) {
            console.log(new Date() + ' - Request: ' + request.url);
            router(request, response, function() {});
        }
    };
};