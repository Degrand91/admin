'use strict';
module.exports = function() {
    let dbManager = require('./dbConnection')();
    const usersCollection = 'test';

    function findDocuments(collection, query, filters, callback) {
        collection.find(query, filters).toArray(function (collectionError, result) {
            if (collectionError) console.log('Error on db query - ' + collectionError);
            return callback(collectionError, result);
        });
    }

    function findUserByUsername(username, callback) {
        dbManager.getConnection("test", function (dbError, db) {
            if (dbError) return callback({});
            let query = { username : username };
            let filters = {_id : 0};
            let collection = db.collection(usersCollection);
            if (!collection) {
                console.log('Error getting collection - ' + usersCollection);
                return callback({});
            }
            findDocuments(collection, query, filters, userFound);
        });

        function userFound(error, result) {
            if (error || (result && result.length == 0)) return callback({});
            return callback(result[0]);
        }
    }

    function registerUser(userData, callback) {
        
        dbManager.getConnection("test", function (err, db) {
            if (err) return callback(JSON.stringify({error:1, message: err}));
            db.collection("test").findOne({ $or: [{ "email": userData.email }, { "username": userData.username }] }, {}, function (err, result) {
                if (err) return callback(JSON.stringify({"status":-2,"message":"server busy try again"}));
                else if (result) {
                    return callback(JSON.stringify({"status":-2,"message":"user already present"}));
                } else {
                    db.collection("test").insert({"email":userData.email,"username":userData.username,"password":userData.password},{},function(err,insRes){
                        if (err) return callback(JSON.stringify({"status":-3,"message":"server busy! try again"}));
                        return callback(JSON.stringify({"status":1,"message":"registration complete"}));
                    });
                }
            })
        });
    }

    return {
        findUserByUsername: findUserByUsername,
        registerUser: registerUser
    }
};