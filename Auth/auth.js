'use strict';
module.exports = function() {
    let bcrypt = require('bcrypt');
    let jwt = require('jsonwebtoken');
    let uuid = require('uuid');
    let usersDb = require('./usersDb')();
    let JWT_DATA = { "secret": "Ug9KYHV907qhFhZ2VM.TdLELUUqY.zie", "issuer": "ADMIN", "algorithm": "HS256" };



    function authenticate(username, password, callback) {
        usersDb.findUserByUsername(username, function (user) {
            if (!user.password) return callback(Error('No login, no user found - ' + username), null);
            validatePassword(user, password, passwordValidated);
        });

        function passwordValidated(error, response) {
            if (error) {
                console.log('Error checking password - ' + error);
                return callback(Error('No login, error checking password'), null);
            } else if (!response.valid) {
                console.log('Wrong password provided for user - ' + username);
                return callback(Error('No login, wrong password provided'), null);
            } else {
                exportUser(response.user, userExported);
            }
        }

        function userExported(error, readOnlyUser) {
            let token = generateJWT(readOnlyUser);
            return callback(null, token);
        }

    }

    function validatePassword(user, plainPassword, callback) {
        bcrypt.compare(plainPassword, user.password, function(err, res) {
            return callback(err, {user : user, valid : res});
        });
    }

    function generateJWT(user) {
        let jwtId = uuid.v4();
        return jwt.sign(user, JWT_DATA.secret, { jwtid : jwtId, algorithm : JWT_DATA.algorithm, issuer : JWT_DATA.issuer, expiresIn: '1d' });
    }

    function exportUser(user, callback) {
        // # todo expand this with more checks
        delete user._id;
        delete user.password;
        callback(null, user);
    }


    /* validation  */
    function isAuthenticated(req, res, next) {
        let token = req.jsonBody.adminToken;
        if (!token) return sendFailedValidation(res, Error('No admin token provided'));
        validateToken(token, authenticatedComplete);

        function authenticatedComplete(error, result) {
            if (error || (result && !result.valid)) sendFailedValidation(res, error);
            else                                    res.end(JSON.stringify({error : false}));
        }
    }

    function validateToken(token, callback) {
        if (!token) return callback(Error('No token provided'), null);
        let validationOptions = { algorithms : [JWT_DATA.algorithm], issuer : JWT_DATA.issuer };
        jwt.verify(token, JWT_DATA.secret, validationOptions, function (error, decoded) {
            if (error) {
                console.log('Token validation failed - ' + error);
                callback(Error('Token validation failed'), null);
            } else {
                console.log('Token validation completed');
                callback(null, {valid : true});
            }
        });
    }

    function sendFailedValidation(res, error) {
        console.log('validation failed: ' + error);
        return res.end(JSON.stringify({error : true}));
    }

    return {
        authenticate: authenticate,
        isAuthenticated : isAuthenticated
    }
};