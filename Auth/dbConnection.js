"use strict";
const mongodb = require('mongodb');
const secretSettings = require('./secretSettings.json');

function dbManager() {
    let connections = {};
    const MongoClient = mongodb.MongoClient;
    
    /* replica set options if necessary*/
    const options = {
        replicaSet: secretSettings.replicaSet,
        readPreference  : secretSettings.readPreference, 
        poolSize: 10,  // number of cached connection available, need to test more to define best value for this parameter - default is 4
        autoReconnect: true
    }; 
    
    /* create mongodb url based on secretSetting parameters */
    function getConnectionURI(database) {
        let uri = "mongodb://"; // mongodb://[username:password@]host1[:port1][,host2[:port2],...[,hostN[:portN]]][/[database][?options]]
        if (!secretSettings || (secretSettings && !Array.isArray(secretSettings.dbServers) && secretSettings.dbServers.length === 0)) return Error("The secret settings are not defined");
        let databaseCredentials = secretSettings.dbCredentials[database];
        if (databaseCredentials.auth && databaseCredentials.username && databaseCredentials.password) uri += encodeURIComponent(databaseCredentials.username) + ":" + encodeURIComponent(databaseCredentials.password) + "@";
        if (secretSettings.dbServers) uri += secretSettings.dbServers.join(",");
        uri += "/" + database        
        return uri;
    }

    function getConnection(database, returnConnection) {
        let uri = getConnectionURI(database);      
        if (uri instanceof Error) return returnConnection(uri, null);
        checkAvailabilityConnection(uri, database,checkComplete);
        function checkComplete(err, client) {
            checkAvailabilityConnection(uri, database, returnConnection)
        }
    }
    
    /* check if a connection is available, else create a new one */
    function checkAvailabilityConnection(uri, database, complete) {
        if (connections[uri] &&  connections[uri].db(database).serverConfig.isConnected()) complete(null, connections[uri].db(database));
        else connectToMongoDB(uri, complete);  
    }
    /* create new mongodb connection */
    function connectToMongoDB(uri, complete) {    
        MongoClient.connect(uri, options, function (err, client) {
            if (!err && client) connections[uri] = client;
            else delete connections[uri];
            complete(err,  connections[uri]);
        });
    }

    return {
        getConnection : getConnection,
    }
}

module.exports = dbManager;